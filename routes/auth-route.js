const routes = require('express').Router();
const auth = require('../controller/auth/auth-controller');

routes.post('/signup', auth.signup);
routes.post('/login', auth.login);

module.exports = routes;