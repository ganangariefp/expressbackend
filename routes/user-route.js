const routes = require('express').Router();
const user = require('../controller/user/user-controller');

routes.get('/', user.getAllUser);

module.exports = routes;