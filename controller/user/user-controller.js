const User = require('../../model/user');

async function getAllUser(req, res) {
	const users = await User.find();
	return res.send(users)
}

module.exports = {
	getAllUser,
}