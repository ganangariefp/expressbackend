const rp = require('request-promise');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const User = require('../../model/user'); 

async function signup(req, res) {
	const username = req.body.username;
	const user = await User.findOne({username});
	
	if (user) {
		const error = {
			"error": {
				"statusCode": 422,
				"code": "ERR_ALREADY_EXIST",
				"message": "user already exist"
			}
		}
		res.status(422)
	
		return res.send(error);
	}
	
	const newUser = await User.insertMany(req.body);

	return res.send(newUser);

	// params = ./users/:id => :id adalah params
	// query = ./users?filter={} => filter={} adalah query
	// header = biasanya token yang diambil
	
}

async function kong(path, body = {}, method = 'GET') {
	const options = {
		uri: 'http://localhost:8001' + path,
		method,
		// uri: path,
		body,
		json: true,
	}
	return rp(options)
}

async function findOrCreateKongUser(username) {
	const consumer = await kong('/consumers/' + username)

	if(!consumer) {
		return createKongUser(username)
	}

	return consumer;
}

async function createKongUser(username) {
	const body = {
		username
	}
	return kong('/consumers', body, 'POST')
	/*{
		"username": "Jason",
		"created_at": 1428555626000,
		"id": "bbdf1c48-19dc-4ab7-cae0-ff4f59d87dc9"
	}*/
}

async function createCredential(username) {
	const body = {
		algorithm:'HS256'
	}
	return kong(`/consumers/${username}/jwt`,body, 'POST');
	// {
  //   "consumer_id": "7bce93e1-0a90-489c-c887-d385545f8f4b",
  //   "created_at": 1442426001000,
  //   "id": "bcbfb45d-e391-42bf-c2ed-94e32946753a",
  //   "key": "a36c3049b36249a3c9f8891cb127243c",
  //   "secret": "e71829c351aa4242c2719cbfbe671c09"
	// }
}

async function getCredential(username) {
	await findOrCreateKongUser(username);
	const credentials = await kong(`/consumers/${username}/jwt`);
	// {
  //   "data": [
  //       {
  //           "rsa_public_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgK .... -----END PUBLIC KEY-----",
  //           "consumer_id": "39f52333-9741-48a7-9450-495960d91684",
  //           "id": "3239880d-1de5-4dbc-bccf-78f7a4280f33",
  //           "created_at": 1491430568000,
  //           "key": "c5a55906cc244f483226e02bcff2b5e",
  //           "algorithm": "RS256",
  //           "secret": "b0970f7fc9564e65xklfn48930b5d08b1"
  //       }
  //   ],
  //   "total": 1
	// }

	if(credentials.total < 1) {
		return createCredential(username)
	}

	return credentials.data[0];
		// {
		//           "rsa_public_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgK .... -----END PUBLIC KEY-----",
		//           "consumer_id": "39f52333-9741-48a7-9450-495960d91684",
		//           "id": "3239880d-1de5-4dbc-bccf-78f7a4280f33",
		//           "created_at": 1491430568000,
		//           "key": "c5a55906cc244f483226e02bcff2b5e",
		//           "algorithm": "RS256",
		//           "secret": "b0970f7fc9564e65xklfn48930b5d08b1"
		// }
	
}

async function createTokenJWT(username) {
	const credential = await getCredential(username);
	const expiresIn = moment().add(1, 'years');
	const payload = {
		username,
		iss: credential.key
	}
	const options = {
		algorithm: credential.algorithm,
		expiresIn: expiresIn.unix(),
	}

	const token = jwt.sign(payload, credential.secret, options);
	return {
		username,
		expiresIn,
		token
	}

}

async function login(req, res) {
	const { username, password } = req.body;
	const user = await User.findOne({username});
	
	if(!user) {
		const error = {
			"error": {
				"statusCode": 404,
				"code": "ERR_NOT_FOUND_USER",
				"message": "user not found"
			}
		}
	
		res.status(404)
	
		return res.send(error);
	}

	const { password: currentPassword } = user;

	if(password !== currentPassword) {
		const error = {
			"error": {
				"statusCode": 401,
				"code": "ERR_LOGIN_FAILES",
				"message": "login failed password missmatch"
			}
		}
	
		res.status(401)
	
		return res.send(error);
	}
	const loginDetail = await createTokenJWT(username)

	return res.send(loginDetail)
}

module.exports = {
	signup,
	login
}