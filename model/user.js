const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  username: {
		type: String,
		min: [6, 'Terlalu pendek boss'],
		max: 12
	},
	password: {
		type: String,
		min: [6, 'Terlalu pendek boss'],
		max: 12
	}
});

const Users = mongoose.model('Users', userSchema);

module.exports = Users;