const express = require('express');
const mongoose = require('mongoose');
const config = require('config');
const bodyParser = require('body-parser');

const routes = require('./routes');

const dbConfig = config.get('db');
const app = express();

mongoose.connect(`mongodb://${dbConfig.host}/${dbConfig.database}`);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function() {
  console.log('db connected')
})

app.use(bodyParser.json());
app.get('/', (req, res) => res.send('Hello World'));
app.use('/users', routes.userRoute);
app.use('/auth', routes.authRoute);


app.listen(3000, () => console.log('run in port 3000'));